package com.restassured.api;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import io.restassured.response.Response;


import static org.testng.Assert.assertEquals;
import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;
import org.testng.Assert;
import io.restassured.http.ContentType;
import io.restassured.internal.path.json.mapping.JsonObjectDeserializer;
import static org.hamcrest.MatcherAssert.*;
import io.restassured.path.json.JsonPath;

import io.restassured.RestAssured.*;
import io.restassured.matcher.RestAssuredMatchers.*;
import org.hamcrest.Matchers.*;

import org.junit.jupiter.api.BeforeEach;


import com.restassured.api.Requests;

import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.Test;
import io.restassured.RestAssured;
class Base{
	@BeforeAll
	public static void init() {
		RestAssured.baseURI = "https://swapi.dev/api/";
//		RestAssured.port=8080; //commented as our API doesnot use port
		//RestAssured.basePath = "sample/api/product";
	}
}




public class Test01_GET {




	@Test(priority = 1)
	void test_001() {

		//All Api should return 200

		Requests a = new Requests();
        a.apishouldwork("/people/");
		a.apishouldwork("/planets/");
		a.apishouldwork("/starships/");



		// given()
		// .get("https://swapi.dev/api/people/")
		// .then()
		// .statusCode(200)
		// .log().all();
	}

	// @Test
	// void test_02() {
		
	// 	given()
	// 		.get("https://swapi.dev/api/people/1")
	// 	.then()
	// 		.statusCode(200)
	// 		.body("data.name", equalTo("Luke Skywalker"));
	// }

	@Test
	void test_002() {

		//First person of people's name should be "luke Skywalker"

		Requests a = new Requests();
		Response responsetwo = a.chooseapiandxthelementasresponse("/people/", "1");
		System.out.println(responsetwo.asString());
		String myresponseget = responsetwo.asString();
		myresponseget.contains("Luke Skywalker");
		System.out.println(responsetwo.getBody().asString());
		
		// Response response = get("https://swapi.dev/api/people/1");
		
		// System.out.println(response.asString());
		// System.out.println(response.getBody().asString());

}

	@Test 
	void test_003(){
		//We assert the properities of APIs easily via equalality method

		Requests a = new Requests();
		a.apiselementpropertyvalidation("/people/", "1","name","Luke Skywalker");
		a.apiselementpropertyvalidation("/people/", "1","mass","77");
		a.apiselementpropertyvalidation("/people/", "1","height","172");
		a.apiselementpropertyvalidation("/starships/", "9","name","Death Star");


	// given()
	// .get("https://swapi.dev/api/people/1")
	// .then()
	// .body("mass", equalTo("77"));


	// given()
	// .get("https://swapi.dev/api/people/1")
	// .then()
	// .body("height", equalTo("172"));

	// given()
	// .get("https://swapi.dev/api/people/1")
	// .then()
	// .body("name", equalTo("Luke Skywalker"));

	
}

	@Test
	void test_004(){
		given()
		.get("")
		.then()
		.statusCode(200)
		.log().all();
	


	


		// given()
		// .get("https://swapi.dev/api/planets/")
		// .then()
		// .body("results.name",  hasItems("Tatooine","Alderaan","Yavin IV","Hoth","Dagobah","Bespin","Endor","Naboo","Coruscant","Kamino"));
	}

@Test 
	void test_07(){
		when().
       	get("https://swapi.dev/api/planets/").
		then().
       body("results.findAll { it.gravity == '1.5 (surface), 1 standard (Cloud City)'}.population", hasItems("6000000", "6000000"));
 

	}

	@Test 
	void test_08(){
		when().
       	get("https://swapi.dev/api/planets/").
		then().
       	body("results.name.collect { it.length() }.sum()", greaterThan(59));
		
 

	}


	@Test 
	void test_09(){
		Requests a = new Requests();
            a.helloworld();
		
 

	}

	@Test 
	void test_10(){
		Requests b = new Requests();
            b.apiworks("https://swapi.dev/api/people/");
		
 

	}


	@Test 
	void test_11(){
		//Getting Response Data
	byte[] byteArray = get("https://swapi.dev/api/people/").asByteArray();
	String json = get("https://swapi.dev/api/people/").asString();

 

	}

	@Test 
	void test_12(){

	
	when().
      get("https://swapi.dev/api/people/").
	then().
      time(lessThan(20000L)); // Milliseconds

 

	}








 	// @Test (groups= "people")
    // public void lukeSkywalkerTest() {
    //     Response getLuke = request.getPeople("/1");
    //     assertEquals(getLuke.getStatusCode(), 200);
    //     String name = getLuke.path("name");
    //     assertEquals(name.toLowerCase(), "luke skywalker");

    //     String lukesHomeworld = getLuke.path("homeworld");
    //     logger.warn("homeworld: " + lukesHomeworld);
    //     String planet = common.getRegMatch(lukesHomeworld);

    //     Response getPlanet = request.getPlanets(planet);
    //     String pn = getPlanet.path("name");
    //     assertEquals(pn, "Tatooine");
    // }



}
