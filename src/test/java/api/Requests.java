package com.restassured.api;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;
import io.restassured.response.Response;


import static org.testng.Assert.assertEquals;
import java.util.HashMap;
import java.util.Map;
import org.json.simple.JSONObject;
import org.testng.Assert;
import io.restassured.http.ContentType;
import io.restassured.internal.path.json.mapping.JsonObjectDeserializer;
import static org.hamcrest.MatcherAssert.*;
import io.restassured.path.json.JsonPath;

import io.restassured.RestAssured.*;
import io.restassured.matcher.RestAssuredMatchers.*;
import org.hamcrest.Matchers.*;






public class Requests {

    public void helloworld() {
    System.out.println("I just got executed!");
  }



  public void apiworks(String url) {

		given()
		.get(url)
		.then()
		.statusCode(200)
		.log().all();
	}



	 public void apishouldwork(String people) {

		String url= "https://swapi.dev/api" + people;  
   		System.out.println(url);

		given()
		.get(url)
		.then()
		.statusCode(200)
		.log().all();
		
	}

		public Response chooseapiandxthelementasresponse(String people, String element) {

		String url= "https://swapi.dev/api" + people + element+"/";  
   	
		Response response = given()
		.when()
		.get(url)
		.then()
        .extract()
		.response();
		return response;
		
	}

		public void apiselementpropertyvalidation(String api, String element, String property, String correctvalue) {

		String url= "https://swapi.dev/api" + api + element+"/";  
   	
			given()
			.get(url)
			.then()
			.body(property, equalTo(correctvalue));
		
	}
}

// package com.restassured.api;
// import io.restassured.builder.RequestSpecBuilder;
// import io.restassured.response.Response;
// import io.restassured.specification.RequestSpecification;
// import utilities.Urls;

// import static io.restassured.RestAssured.given;

// public class Requests {


  
//     private RequestSpecification requestSpec;
//     private Urls url = new Urls();

//     public void requestSpec() {
//         requestSpec = new RequestSpecBuilder()
//                 .setBaseUri(url.BASE)
//                 .setContentType("application/json")
//                 .build();
//     }

//     public Response getFilms(String filmNumber) {
//         requestSpec();
//         Response response = given()
//                 .spec(requestSpec)
//                 .when()
//                 .get(url.FILM + filmNumber)
//                 .prettyPeek()
//                 .then()
//                 .extract()
//                 .response();
//         return response;
//     }


//     public Response getPlanets(String planetNumber) {
//         requestSpec();
//         Response response = given()
//                 .spec(requestSpec)
//                 .when()
//                 .get(url.PLANETS + planetNumber)
//                 .then()
//                 .extract()
//                 .response();
//         return response;
//     }

//     public Response getPeople(String peopleNumber) {
//         requestSpec();
//         Response response = given()
//                 .spec(requestSpec)
//                 .when()
//                 .get(url.PEOPLE + peopleNumber)
//                 .then()
//                 .extract()
//                 .response();
//         return response;
//     }

//     public Response getSpecies(String speciesNumber) {
//         requestSpec();
//         Response response = given()
//                 .spec(requestSpec)
//                 .when()
//                 .get(url.SPECIES + speciesNumber)
//                 .then()
//                 .extract()
//                 .response();
//         return response;
//     }

//     public Response getStarships(String shipNumber) {
//         requestSpec();
//         Response response = given()
//                 .spec(requestSpec)
//                 .when()
//                 .get(url.STARSHIPS + shipNumber)
//                 .then()
//                 .extract()
//                 .response();
//         return response;
//     }


//     public Response getVehicles(String vehicleNumber) {
//         requestSpec();
//         Response response = given()
//                 .spec(requestSpec)
//                 .when()
//                 .get(url.VEHICLES + vehicleNumber)
//                 .then()
//                 .extract()
//                 .response();
//         return response;
//     }
// }
